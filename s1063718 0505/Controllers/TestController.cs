﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace s1063718_0505.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            ViewData["aaa"] = "123";
            ViewBag.Name = "Benson Wang";

            ViewData["A"] = 2;
            ViewData["B"] = 4;

            ViewBag.A = 2;
            ViewBag.B = 4;
            return View();

        }
        public ActionResult Html()
        {
            return View();
        }
        public ActionResult HtmlHelper()
            {
                return View();
            }

        public ActionResult Razor()
        {
            return View();
        }

    }
}