﻿using s1063718_0505.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace s1063718_0505.Controllers
{
    public class BMIController : Controller
    {
        public ActionResult Index()
        {
            return View(new BMIData());
        }

        // GET: BMI
        [HttpPost]
        public ActionResult Index(BMIData data)
        {
            if(ModelState.IsValid)
            if (data.身高 != null && data.體重 != null)
            {
                var m_身高 = data.身高 / 100;
                var result = data.體重 / (m_身高 * m_身高);


                data.Result = result;


                var level = "";
                if(data.身高 < 50 || data.身高 > 200)
                {
                    ViewBag.HeightError = "身高請輸入50~200的數值";
                }
                if (data.體重 < 50 || data.體重 > 200)
                {
                    ViewBag.WeightError = "體重請輸入30~150的數值";
                }
                if (result < 18.5)
                {
                    level = "體重過輕";
                }
                else if (result > 18.5 && result < 24)
                {
                    level = "正常範圍";
                }
                else if (result > 24 && result < 27)
                {
                    level = "過重";
                }
                else if (result > 27 && result < 30)
                {
                    level = "輕度肥胖";
                }
                else if (result > 30 && result < 35)
                {
                    level = "中度肥胖";
                }
                else if (result >= 35)
                {
                    level = "重度肥胖";
                }

              
                data.Level = level;
                
            }
            return View(data);
        }

    }
}