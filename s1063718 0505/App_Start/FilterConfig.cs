﻿using System.Web;
using System.Web.Mvc;

namespace s1063718_0505
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
