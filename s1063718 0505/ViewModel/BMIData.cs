﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace s1063718_0505.ViewModel
{
    public class BMIData
    {
        [Display(Name = "體重")]
        [Required(ErrorMessage = "阿你的體重呢?")]
        [Range(30,150, ErrorMessage = "請輸入30-150的數值")]

        public float? 體重 { get; set; }
        [Display(Name = "身高")]
        [Required(ErrorMessage = "阿你的身高呢?")]
        [Range(50, 200, ErrorMessage = "請輸入50-200的數值")]
        public float? 身高 { get; set; }
        public float? Result { get; set; }
        public string Level { get; set; }



    }
}